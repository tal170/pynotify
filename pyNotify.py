import smtplib
import credentials
import logging

from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText

g_logger = logging.getLogger(__name__)

GMAIL_SMTP_ADDRESS = "smtp.gmail.com"
GMAIL_SMTP_PORT = 587

class Notify(object):
	def __init__(self):
		pass
	
	def send_notification(self, target, header, content):
		raise NotImplementedError()

class GmailNotify(Notify):
	username = None
	password = None
	server = None
	
	def __init__(self, username=credentials.USERNAME, password=credentials.PASSWORD):
		self.username = username
		self.password = password
		self.login(username, password)
		
	def login(self, username, password):
		if self.server:
			try:
				self.server.quit()
			except:
				pass
		g_logger.info("Logging into gmail account: %s" % username)
		self.server = smtplib.SMTP(GMAIL_SMTP_ADDRESS, GMAIL_SMTP_PORT)
		self.server.ehlo()
		self.server.starttls()
		self.server.login(self.username, self.password)
	
	def send_notification(self, target, header, content):
		msg = MIMEMultipart()
		
		msg['From'] = "Notification Service <%s@gmail.com>" % self.username 
		msg['To'] = target
		msg['Subject'] = header
		 
		msg.attach(MIMEText(content, 'plain'))
		g_logger.info("Sending email to: %s with subject: \"%s\" and content: \n\"%s\"" % (target, header, content))
		self.server.sendmail("Personal Notification <" + self.username + "@gmail.com>", target, msg.as_string())

def main():
	GmailNotify().send_notification("@gmail.com", "Notification: Apartment found in Tel Aviv", "Just kidding!")
if __name__ == "__main__":
	main()